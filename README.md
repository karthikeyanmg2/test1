# MessageBoard

A simple message board application written in Java with Spring Boot and Maven

## Versions

Version 1.0 is intended for demo use only

## Usage
To use this application, either compile it and run the jar or run it with mvn springt-boot:run

Two testusers, testuser1 and testuser2, are added to the application during startup.
No add user endpoint exist, so the two users should be used to test the use of the demo application. 
testuser1 has password testpass1 and testuser 2 has password testpass2

To be able to call the messages endpoint, one first need to authenticate the user.
This is done by calling the /authenticate endpooint with for example curl or Postman
Then use the token received as authentication header.

**Calling the api**

Authentication:
curl -s -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' --data '{"username":"testuser1","password":"testpass1"}' http://localhost:8080/authenticate

GET all messages:
curl -v -H -X GET 'Accept: application/json' -H "Authorization: Bearer {token here}" localhost:8080/api/messages

POST a message:
curl -v -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' --data '{"title":"Updated", "text":"New text" }' -H "Authorization: Bearer {token here}" http://localhost:8080/api/messages

PUT (update) a message:
curl -v -X PUT -H 'Accept: application/json' -H 'Content-Type: application/json' --data '{ "id":1, "author":"testuser1", "title":"Updated", "text":"New text" }' -H "Authorization: Bearer {token here}" http://localhost:8080/api/messages

DELETE a message:
curl -X DELETE -H "Authorization: Bearer {token here}" http://localhost:8080/api/messages/1

**API docs**

A swagger desciption of the contract can be found at http://localhost:8080/v2/api-docs

A swagger ui can be found at http://localhost:8080/swagger-ui.html