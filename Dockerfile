FROM openjdk:8-jdk-alpine
MAINTAINER Peter Myrén <petermyren@gmail.com>

ADD target/messageboard.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
EXPOSE 8080
