package com.petermyren.messageboard.messages;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import com.petermyren.messageboard.security.AuthenticationHolder;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MessageServiceTest {

    private MessageRepository messageRepository = mock(MessageRepository.class);
    private AuthenticationHolder authenticationHolder = mock(AuthenticationHolder.class);
    private MessageService messageService = new MessageService(messageRepository, authenticationHolder);
    
    @Before
    public void setup() {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getName()).thenReturn("somuser");
        when(authenticationHolder.getAuthentication()).thenReturn(authentication);
    }

    @Test
    public void context_loads() {
        assertNotNull("Could not find Message Service to run tests against", messageService);
    } 
    
    @Test
    public void findMessages_twoReturnedFromRepo_twoReturnedFromService() {
        // GIVEN
        List<Message> messagesFromRepository = mockMessagesFetchedFromRepository();

        // WHEN
        List<Message> messages = messageService.findAllMessages();
        
        // THEN
        assertEquals("Message service did not return same amount of messages as the repository fetched when performing find all messages", messagesFromRepository.size(), messages.size());
    }
    
    @Test
    public void findMessages_messagesExists_returnsMessageWithCorrectId() {
        // GIVEN
        List<Message> messagesFromRepository = mockMessagesFetchedFromRepository();

        // WHEN
        List<Message> messages = messageService.findAllMessages();
        Message message = messages.get(0);
        
        // THEN
        assertEquals("Incorrect message id for message returned by find all in service", 1, message.getId().intValue());
    }
    
    @Test
    public void findMessages_messagesExists_returnsMessageWithCorrectTitle() {
        // GIVEN
        List<Message> messagesFromRepository = mockMessagesFetchedFromRepository();

        // WHEN
        List<Message> messages = messageService.findAllMessages();
        Message message = messages.get(0);
        
        // THEN
        assertEquals("Incorrect message title for message returned by find all in service", "M1", message.getTitle());
    }
    
    @Test
    public void findMessages_messagesExists_returnsMessageWithCorrectText() {
        // GIVEN
        List<Message> messagesFromRepository = mockMessagesFetchedFromRepository();

        // WHEN
        List<Message> messages = messageService.findAllMessages();
        Message message = messages.get(0);
        
        // THEN
        assertEquals("Incorrect message text for message returned by find all in service", "Message 1", message.getText());
    }
    
    @Test
    public void saveMessage_messagesPassed_returnsMessageWithCorrectText() {
        // GIVEN
        Message messageToSave = Message.builder().title("Test message").text("The text for test message").build();
        when(messageRepository.save(any(Message.class), anyString())).thenReturn(messageToSave);

        // WHEN
        Message messageSaved = messageService.saveMessage(messageToSave);
        
        // THEN
        assertEquals("Incorrect message text for message returned after save", messageToSave.getText(), messageSaved.getText());
    }

    private List<Message> mockMessagesFetchedFromRepository() {
        List<Message> messagesFromRepository = Arrays.asList(
                Message.builder().id(1L).title("M1").text("Message 1").build(),
                Message.builder().id(1L).title("M1").text("Message 1").build());
        
        when(messageRepository.findAll()).thenReturn(messagesFromRepository);
        return messagesFromRepository;
    }
}