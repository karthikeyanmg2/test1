package com.petermyren.messageboard.users;

import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import com.petermyren.messageboard.persistence.User;

import java.util.Optional;

@Repository
public interface UserDetailsRepository extends CrudRepository<User, Long> {
    Optional<UserDetails> findByUsername(String username);
}
