package com.petermyren.messageboard.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import com.petermyren.messageboard.messages.Message;

@Mapper(componentModel = "spring")
public interface MessageMapperMapstruct {
    MessageMapperMapstruct INSTANCE = Mappers.getMapper( MessageMapperMapstruct.class );
    
    @Mapping(source = "author.username", target = "author")
    Message entityToMessage(com.petermyren.messageboard.persistence.Message entity);

    @Mapping(target = "author", ignore = true)
    com.petermyren.messageboard.persistence.Message messageToEntity(Message messageToSave);
}
