package com.petermyren.messageboard.security;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class JwtSecurityResponse {
    private final String jwttoken;
}
