package com.petermyren.messageboard.messages;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/messages", produces = "application/json")
@Api(value = "Message api for managing messages")
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    @ApiOperation(value = "Find all messages")
    ResponseEntity<List<Message>> findAllMessages() {
        return ResponseEntity.ok(messageService.findAllMessages());
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add and save a message")
    ResponseEntity<Message> createMessage(@ApiParam(value = "The message to be saved", required = true) 
                                         @RequestBody Message message) throws URISyntaxException {
        Message messageCreated = messageService.saveMessage(message);
        
        return ResponseEntity.created(new URI(String.valueOf(message.getId())))
                .body(messageCreated);
    }
    
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update and save a message")
    ResponseEntity<Message> updateMessage( @ApiParam(value = "The message to be updated", required = true)
                                           @RequestBody Message message) throws MessageDontBelongToYouException {
        Message messageUpdated = messageService.updateMessage(message);
        return ResponseEntity.ok(messageUpdated);
    } 
    
    @DeleteMapping("/{messageId}")
    @ApiOperation(value = "Delete a message based on message id")
    ResponseEntity deleteMessage( @ApiParam(value = "Id of the message to be deleted", required = true)
                                  @PathVariable Long messageId) throws MessageDontBelongToYouException {
        messageService.deleteMessage(messageId);
        return ResponseEntity.ok("Message deleted");
    }
}
