package com.petermyren.messageboard.messages;

import org.springframework.stereotype.Component;
import com.petermyren.messageboard.mapping.MessageMapper;
import com.petermyren.messageboard.persistence.MessageCrudRepositoryAdapter;
import com.petermyren.messageboard.persistence.User;
import com.petermyren.messageboard.users.UserRepository;

import java.util.List;

@Component
public class MessageRepository {

    private final MessageMapper messageMapper;
    private final MessageCrudRepositoryAdapter messageCrudRepositoryAdapter;
    private final UserRepository userRepository;
    
    public MessageRepository(MessageMapper messageMapper, MessageCrudRepositoryAdapter messageCrudRepositoryAdapter, UserRepository userRepository) {
        this.messageMapper = messageMapper;
        this.messageCrudRepositoryAdapter = messageCrudRepositoryAdapter;
        this.userRepository = userRepository;
    }

    public List<Message> findAll() {
        Iterable<com.petermyren.messageboard.persistence.Message> messagesFromDb = messageCrudRepositoryAdapter.findAll();
        return messageMapper.mapFromEntitiesToMessages(messagesFromDb);
    }

    public Message save(Message messageToSave, String username) {
        com.petermyren.messageboard.persistence.Message messageEntity = messageMapper.mapToEntity(messageToSave);
        User user = userRepository.findByUsername(username);
        messageEntity.setAuthor(user);
        com.petermyren.messageboard.persistence.Message saved = messageCrudRepositoryAdapter.save(messageEntity);
        return messageMapper.mapFromEntityToMessage(saved);
    }

    public void delete(Long messageId, String username) throws MessageDontBelongToYouException {
        com.petermyren.messageboard.persistence.Message messageToDelete = messageCrudRepositoryAdapter.findById(messageId)
                .orElseThrow(MessageDontExistException::new);
        validateUSerOwnsMessage(username, messageToDelete);
        messageCrudRepositoryAdapter.delete(messageToDelete);
        
    }

    public Message updateMessage(Message message, String username) {
        com.petermyren.messageboard.persistence.Message messageToUpdate = messageCrudRepositoryAdapter.findById(message.getId())
                .orElseThrow(MessageDontExistException::new);
        validateUSerOwnsMessage(username, messageToUpdate);
        messageToUpdate.setTitle(message.getTitle());
        messageToUpdate.setText(message.getText());
        return messageMapper.mapFromEntityToMessage(messageCrudRepositoryAdapter.save(messageToUpdate));
    }

    private void validateUSerOwnsMessage(String username, com.petermyren.messageboard.persistence.Message messageToUpdate) {
        if (!messageToUpdate.getAuthor().getUsername().equals(username)) {
            throw new MessageDontBelongToYouException("Cannot find a message with this id for current user");
        }
    }
}
