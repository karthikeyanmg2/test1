package com.petermyren.messageboard.messages;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Getter
@Builder
public class Message {
    @ApiModelProperty(notes = "Id of the message", example = "1")
    private Long id;
    
    @ApiModelProperty(notes = "Author of the message, same as username", example = "myusername")
    private String author;
    
    @ApiModelProperty(notes = "Title of the message. Must not be null", example = "My first message")
    @NonNull 
    private String title;
    
    @ApiModelProperty(notes = "Text of the message. Must not be null", example = "This is my first message on the message board")
    @NonNull 
    private String text;
}
