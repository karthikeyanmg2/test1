package com.petermyren.messageboard.messages;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import com.petermyren.messageboard.security.AuthenticationHolder;

import java.util.List;

@Service
public class MessageService {
    
    private final MessageRepository messageRepository;
    private final AuthenticationHolder authenticationHolder;

    public MessageService(MessageRepository messageRepository, AuthenticationHolder authenticationHolder) {
        this.messageRepository = messageRepository;
        this.authenticationHolder = authenticationHolder;
    }

    public List<Message> findAllMessages() {
        return messageRepository.findAll();
    }

    public Message saveMessage(Message messageToSave) {
        Authentication authentication = authenticationHolder.getAuthentication();
        String username = authentication.getName();
        
        return messageRepository.save(messageToSave, username);
    }
    
    public void deleteMessage(Long messageId) throws MessageDontBelongToYouException {
        String username = findUsername();
        messageRepository.delete(messageId, username);
    }

    public Message updateMessage(Message message) {
        String username = findUsername();
        return messageRepository.updateMessage(message, username);
    }
    
    private String findUsername() {
        Authentication authentication = authenticationHolder.getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        return principal.getUsername();
    }

}
